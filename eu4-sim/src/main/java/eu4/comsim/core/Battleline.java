package eu4.comsim.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import eu4.comsim.core.datatypes.Phase;
import eu4.comsim.core.datatypes.Terrain;
import eu4.comsim.core.datatypes.UnitType;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Represents the forces actually deployed on the field during a battle
 *
 */
@Slf4j
public class Battleline {
	
	public final Army army;
	public final Terrain terrain;
	/** combat combatWidth (influenced by mil tech level, modifiers, and opponent) */
	private final int combatWidth;
	@Getter private boolean isAttacker;
	
	// Keep all individual stats so we can do statistical analysis later
	public final List<BattleStats> history = new ArrayList<>();
	
	// Variables that control during deployment how much cavalry spaces are
	// reserved on the side
	private static int SIDE_RESERVED_FRONTROW = 2;
	private static int SIDE_RESERVED_BACKROW = 2;
	
	public List<Regiment> frontRow;
	public List<Regiment> backRow;
	
	private Collection<Regiment> allRegiments;
	
	// Queues of units available for deployment (standby)
	Iterator<Regiment> infantry;
	Iterator<Regiment> cavalry;
	Iterator<Regiment> artillery;
	
	/**
	 * Creates a battleline. Initializes regiments and deploys them.
	 */
	public Battleline(Army army, int combatWidth, Terrain terrain, boolean isAttacker) {
		this.army = army;
		this.terrain = terrain;
		this.combatWidth = combatWidth;
		this.isAttacker = isAttacker;
		
		frontRow = new ArrayList<>(Collections.nCopies(combatWidth, Regiment.EMPTY));
		backRow = new ArrayList<>(Collections.nCopies(combatWidth, Regiment.EMPTY));
		
		this.allRegiments = army.getRegiments();
		infantry = Regiment.getRegiments(allRegiments, UnitType.INFANTRY).iterator();
		cavalry = Regiment.getRegiments(allRegiments, UnitType.CAVALRY).iterator();
		artillery = Regiment.getRegiments(allRegiments, UnitType.ARTILLERY).iterator();
		
	}
	
	/**
	 * @param defender Opponent
	 * @param phase Current phase
	 * @param roll Base roll
	 */
	public void attackAll(Battleline defender, Phase phase, int roll) {
		
		double shockMult = (phase == Phase.SHOCK) ? getShockMultiplier() : 1.0;
		double defTactics = defender.getTactics();
		for (Regiment attackingRegiment : allAttackingRegiments()) {
			int position = frontRow.contains(attackingRegiment) ? frontRow.indexOf(attackingRegiment)
					: backRow.indexOf(attackingRegiment);
			Regiment defendingRegiment = defender.findTarget(position, attackingRegiment.getFlankingRange());
			if (defendingRegiment == Regiment.EMPTY) {
				continue;
			}
			Regiment behind = defender.backRow.get(defender.frontRow.indexOf(defendingRegiment));
			boolean flanking = defender.frontRow.indexOf(defendingRegiment) != position;
			double offModifierFromTech = attackingRegiment.tech.casualtyMultiplier(attackingRegiment.unit.type, phase);
			double offArtMult = attackingRegiment.isOfType(UnitType.ARTILLERY) && backRow.contains(attackingRegiment)
					? 0.5
					: 1;
			double defArtMult = defendingRegiment.isOfType(UnitType.ARTILLERY) ? 2 : 1;
			// Total multiplier takes everything into account that is not derived from pips
			double totalMult = offModifierFromTech * shockMult * offArtMult * defArtMult / defTactics;
			attackingRegiment.attack(defendingRegiment, phase, roll, behind, totalMult, flanking);
		}
		
	}
	
	/**
	 * Target finding routine <br>
	 * TODO Determine selection heuristics for multiple legal targets and implement it
	 *
	 * @param position index representing a position in the battle line.
	 * @param flankingRange maximum difference between positions the unit can target.
	 * @return the target regiment if there is an eligible target, <code>null</code> otherwise
	 */
	public Regiment findTarget(int position, int flankingRange) {
		Comparator<Regiment> nearestToPosition = Comparator.comparingInt(r -> Math.abs(frontRow.indexOf(r) - position));
		//@formatter:off
		return frontRow.stream()
				.filter(Regiment.IS_EMPTY.negate())
				.sorted(nearestToPosition)
				.findFirst()
				.orElse(Regiment.EMPTY);
		//@formatter:on
	}
	
	/**
	 * @return All regiments
	 */
	public Collection<Regiment> getAllRegiments() {
		return allRegiments;
	}
	
	/**
	 * <li>Removes regiments whose morale has been reduced to zero from its position
	 * <li>{@link #backRow} units move to the {@link #frontRow} when frontRow is empty <br>
	 * <li>Attempt to fill empty positions with unused regiments from queue<br>
	 *
	 */
	public void consolidate() {
		// Remove all routed regiments
		log.debug("Front before replace: {}", frontRow);
		frontRow.replaceAll(r -> r.defeated() ? Regiment.EMPTY : r);
		log.debug("Front after replace: {}", frontRow);
		backRow.replaceAll(r -> r.defeated() ? Regiment.EMPTY : r);
		// Shuffle up back row
		// TODO Determine: Do sometimes frontRow units shuffle over in empty
		// spaces instead of pulling the backRow to the front?
		for (int i = 0; i < frontRow.size(); i++) {
			if (frontRow.get(i) == Regiment.EMPTY) {
				frontRow.set(i, backRow.get(i));
				backRow.set(i, Regiment.EMPTY);
			}
		}
		closeRanks();
		log.debug("Front after shuffle: {}", frontRow);
		// Attempt to fill empty positions with unused regiments from queue
		// (these have taken morale damage while standing by!)
		deploy();
		log.debug("Front after (re)deploy: {}", frontRow);
	}
	
	private void closeRanks() {
		int middle = combatWidth / 2;
		List<Regiment> active = frontRow.stream().filter(Regiment.IS_EMPTY.negate()).collect(Collectors.toList());
		int start = middle - active.size() / 2;
		frontRow.replaceAll(r -> Regiment.EMPTY);
		for (Regiment r : active) {
			frontRow.set(start++, r);
		}
	}
	
	/**
	 * @return Regiments that match condition: morale > 0 && strength > 0 && in front row OR artillery in back row
	 */
	public Collection<Regiment> allAttackingRegiments() {
		Predicate<Regiment> canAttack = r -> !r.defeated()
				&& (frontRow.contains(r) || (backRow.contains(r) && r.isOfType(UnitType.ARTILLERY)));
		return allRegiments.stream().filter(canAttack).collect(Collectors.toList());
	}
	
	/**
	 * @return Current tactics from technology, potentially modified by dynamically computed cav support penalty
	 */
	public double getTactics() {
		return (hasInsufficientSupport() ? 0.75 : 1.0) * allRegiments.stream().findAny().get().tech.getTactics();
		
	}
	
	/**
	 * @return <code>true</code> if combined strength of cavalry (individual soldiers) percentage of total strength
	 *         exceeds the maxCavSupportRatio, <code>false</code> otherwise
	 */
	public boolean hasInsufficientSupport() {
		
		int total = allRegiments.stream().mapToInt(Regiment::getStrength).sum();
		int cav = allRegiments.stream().filter(r -> r.isOfType(UnitType.CAVALRY)).mapToInt(Regiment::getStrength).sum();
		return army.getTechnology().getGroup().maxCavalryRatio < (double) cav / (double) total;
	}
	
	/**
	 * Deploys available Regiments to empty battlefield locations.
	 *
	 * @see <a href="http://www.eu4wiki.com/Land_warfare#Unit_deployment">http:/
	 *      /www.eu4wiki.com/Land_warfare#Unit_deployment</a>
	 */
	public void deploy() {
		
		while (hasSpace(frontRow) && (cavalry.hasNext() || infantry.hasNext())) {
			Optional<Integer> nearestEmptyPosition = getNearestPosition(frontRow, Regiment.IS_EMPTY);
			if (nearestEmptyPosition.isPresent()) {
				int position = nearestEmptyPosition.get();
				boolean outside = position < SIDE_RESERVED_FRONTROW
						|| position > (combatWidth - SIDE_RESERVED_FRONTROW);
				Regiment r = (infantry.hasNext() && !outside) ? infantry.next()
						: cavalry.hasNext() ? cavalry.next() : infantry.next();
				frontRow.set(position, r);
			}
		}
		while (hasSpace(backRow) && (artillery.hasNext() || infantry.hasNext() || cavalry.hasNext())) {
			Optional<Integer> nearestEmptyPosition = getNearestPosition(backRow, Regiment.IS_EMPTY);
			if (nearestEmptyPosition.isPresent()) {
				int position = nearestEmptyPosition.get();
				boolean outside = position < SIDE_RESERVED_BACKROW || position > (combatWidth - SIDE_RESERVED_BACKROW);
				if (outside && artillery.hasNext() && hasSpace(frontRow)) {
					frontRow.set(getNearestPosition(frontRow, Regiment.IS_EMPTY).get(), artillery.next());
					continue;
				}
				Regiment r = artillery.hasNext() ? artillery.next()
						: infantry.hasNext() ? infantry.next() : cavalry.next();
				backRow.set(nearestEmptyPosition.get(), r);
				
			}
		}
	}
	
	private Optional<Integer> getNearestPosition(List<Regiment> row, Predicate<Regiment> condition) {
		int middle = combatWidth / 2;
		Comparator<Integer> distanceToMiddle = Comparator.comparingInt(r -> Math.abs(r - middle));
		// nearest empty position
		//@formatter:off
		return IntStream.range(0, row.size())
				.boxed()
				.filter(i -> condition.test(row.get(i)))
				.collect(Collectors.minBy(distanceToMiddle));
		//@formatter:on
	}
	
	private boolean hasSpace(List<Regiment> row) {
		return getNearestPosition(row, Regiment.IS_EMPTY).isPresent();
	}
	
	/**
	 * @return Battleline where all front/backrow positions are copied directly instead of calling manual deploy().
	 *         Useful for playback/record keeping
	 */
	public Battleline copy() {
		List<Regiment> regimentsCopied = allRegiments.stream().map(Regiment::copy).collect(Collectors.toList());
		Army armyCopy = new Army(army.getGeneral(), army.getTechnology(), regimentsCopied);
		armyCopy.setDieRollProvider(army.getDieRollProvider());
		Battleline copy = new Battleline(armyCopy, combatWidth, terrain, isAttacker);
		copy.frontRow = frontRow.stream().map(Regiment::copy).collect(Collectors.toList());
		copy.backRow = backRow.stream().map(Regiment::copy).collect(Collectors.toList());
		return copy;
	}
	
	/**
	 * @return Determines multiplier in shock phases (affects hordes on flat terrains)
	 */
	private double getShockMultiplier() {
		if (army.getTechnology().getGroup() == Technology.Group.NOMAD) {
			// Nomads do +25% damage in flat terrain, and -25% damage in non-flat terrain
			return (terrain.attackerPenalty == 0) ? 1.25 : 0.75;
		}
		return 1;
	}
}
