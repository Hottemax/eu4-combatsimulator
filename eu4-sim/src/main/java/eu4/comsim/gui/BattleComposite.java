package eu4.comsim.gui;

import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;

import com.google.common.eventbus.Subscribe;

import eu4.comsim.core.Battle;
import eu4.comsim.core.BattleUpdateEvent;
import eu4.comsim.core.Regiment;
import eu4.comsim.core.Util;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings("synthetic-access")
public class BattleComposite {
	
	private static int ICON_PIXEL_DIMENSION = 15;
	
	Label[] icons = new Label[160];
	private Composite swtComposite;
	
	/**
	 * Create the composite.
	 */
	public BattleComposite() {
		Util.EVENTBUS.register(this);
	}
	
	private Label createLabel() {
		Label regimentIcon = new Label(swtComposite, SWT.BORDER | SWT.CENTER);
		regimentIcon.setAlignment(SWT.CENTER);
		GridData gd_lblNewLabel = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		gd_lblNewLabel.heightHint = ICON_PIXEL_DIMENSION;
		gd_lblNewLabel.widthHint = ICON_PIXEL_DIMENSION;
		regimentIcon.setLayoutData(gd_lblNewLabel);
		regimentIcon.setVisible(false);
		return regimentIcon;
	}
	
	@Subscribe
	public void update(BattleUpdateEvent o) {
		Battle battle = o.getBattle();
		
		int day = o.getDay();
		
		Arrays.stream(icons).forEach(i -> {
			i.setImage(null);
			i.setVisible(false);
		});
		int offset = (40 - battle.width) / 2;
		fillIcons(battle.battleLineA.history.get(day).bl.backRow, "blue", offset);
		fillIcons(battle.battleLineA.history.get(day).bl.frontRow, "blue", 40 + offset);
		fillIcons(battle.battleLineB.history.get(day).bl.frontRow, "red", 80 + offset);
		fillIcons(battle.battleLineB.history.get(day).bl.backRow, "red", 120 + offset);
		
	}
	
	private void fillIcons(List<Regiment> regiments, String color, int offset) {
		for (int i = 0; i < regiments.size(); i++) {
			Regiment r = regiments.get(i);
			String unitType = r.unit.type.name;
			Image icon = (r == Regiment.EMPTY) ? null
					: SWTResourceManager.getImage(BattleComposite.class,
							"/icons/battle/" + unitType + "-" + color + ".png");
			icons[offset + i].setImage(icon);
			icons[offset + i].setToolTipText(r.toString());
			icons[offset + i].setData(r);
			icons[offset + i].setVisible(true);
			icons[offset + i].addMouseTrackListener(new MouseTrackListener() {
				
				// TODO Add logic here (display stats/opponent/graph/history for individual regiment
				@Override
				public void mouseHover(MouseEvent e) {
					log.debug("HOVER");
					
				}
				
				@Override
				public void mouseExit(MouseEvent e) {
					log.debug("BYE");
				}
				
				@Override
				public void mouseEnter(MouseEvent e) {
					log.debug("HI");
				}
			});
		}
		
	}
	
	/**
	 * @wbp.factory
	 */
	public Composite create(Composite parent) {
		swtComposite = new Composite(parent, SWT.BORDER);
		swtComposite.setLayout(new GridLayout(40, false));
		icons = new Label[160];
		for (int i = 0; i < 160; i++) {
			Label l = createLabel();
			icons[i] = l;
			icons[i].addMouseListener(new MouseAdapter() {
				
				@Override
				public void mouseDown(MouseEvent e) {
					// TODO make selectable, adapt table/info views
					log.info("clicked on {}", l.getToolTipText());
					super.mouseDown(e);
				}
			});
		}
		swtComposite.setBackgroundMode(SWT.INHERIT_DEFAULT);
		swtComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		GridData gd_battleComposite = new GridData(SWT.CENTER, SWT.CENTER, false, false, 3, 1);
		// gd_battleComposite.widthHint = 900;
		swtComposite.setLayoutData(gd_battleComposite);
		
		return swtComposite;
	}
}
