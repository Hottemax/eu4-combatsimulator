package eu4.comsim.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;

import lombok.extern.slf4j.Slf4j;

/**
 * Main class that houses the Shell and the main() method.
 */
@Slf4j
public class CombatSimulator {
	
	static MainComposite mainComposite;
	
	private static final String VERSION = "1.31";
	private static final String APPLICATION_ICON = "/icons/general/EU4_icon.png";
	private static final int OPTIMAL_HEIGHT = 810;
	private static final int OPTIMAL_WIDTH = 920;
	private static final String HELP_MESSAGE = "Version " + VERSION
			+ " (beta). You can help improve this program by submitting comments, feature requests and bugs at https://bitbucket.org/Hottemax/eu4-combatsimulator/issues";
	
	static Shell shlEuCombatSimulator;
	
	/**
	 * Launch the application.
	 * 
	 * @param args None
	 */
	public static void main(String[] args) {
		shlEuCombatSimulator = createShell();
		
		try {
			Display display = Display.getDefault();
			shlEuCombatSimulator.open();
			shlEuCombatSimulator.layout();
			while (!shlEuCombatSimulator.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			MessageBox messageBox = new MessageBox(new Shell(Display.getDefault()), SWT.ICON_ERROR);
			messageBox.setMessage("Unexpected error:" + e.toString()
					+ "An error.log has been generated. You can help improve this program by submitting bugs at https://bitbucket.org/Hottemax/eu4-combatsimulator/issues");
			messageBox.open();
		}
		
	}
	
	/**
	 * Create contents of the window.
	 */
	private static Shell createShell() {
		shlEuCombatSimulator = new Shell();
		shlEuCombatSimulator.setImage(SWTResourceManager.getImage(MainComposite.class, APPLICATION_ICON));
		shlEuCombatSimulator.setSize(OPTIMAL_WIDTH, OPTIMAL_HEIGHT);
		shlEuCombatSimulator.setMinimumSize(OPTIMAL_WIDTH, OPTIMAL_HEIGHT);
		shlEuCombatSimulator.setText("EU4 Combat Simulator " + VERSION);
		shlEuCombatSimulator.setLocation(100, 50);
		GridLayout gl_shlEuCombatSimulator = new GridLayout(1, false);
		shlEuCombatSimulator.setLayout(gl_shlEuCombatSimulator);
		
		Menu menu = new Menu(shlEuCombatSimulator, SWT.BAR);
		shlEuCombatSimulator.setMenuBar(menu);
		MenuItem mntmHelp = new MenuItem(menu, SWT.CASCADE);
		mntmHelp.setText("Help");
		Menu menu_cascade = new Menu(mntmHelp);
		mntmHelp.setMenu(menu_cascade);
		MenuItem mntmAbout = new MenuItem(menu_cascade, SWT.NONE);
		mntmAbout.setText("About");
		
		mntmAbout.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				MessageBox messageBox = new MessageBox(shlEuCombatSimulator, SWT.ICON_INFORMATION);
				messageBox.setMessage(HELP_MESSAGE);
				messageBox.open();
			}
		});
		// Add the main composite
		mainComposite = MainComposite.create(shlEuCombatSimulator);
		mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		return shlEuCombatSimulator;
	}
	
}
