package eu4.comsim.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.General;

/**
 * Simple Composite containing UI elements for selecting General stats.
 */
public class GeneralGroup {
	
	private static final String NAME = "General";
	/** SWT element */
	private Group swtGroup;
	private Spinner[] pipSpinners = new Spinner[4];
	
	public GeneralGroup(Composite parent, General general) {
		swtGroup = createGroup(parent, NAME, general);
	}
	
	/**
	 * @wbp.factory
	 */
	public Group createGroup(Composite parent, String text, General general) {
		swtGroup = new Group(parent, SWT.NONE);
		swtGroup.setText(text);
		
		RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
		rowLayout.center = true;
		rowLayout.justify = true;
		swtGroup.setLayout(rowLayout);
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		createPipLabel("Fire");
		pipSpinners[0] = createPipSpinner("Select fire pips (0-6). Applied during fire phases.", general.getFire());
		createPipLabel("Shock");
		pipSpinners[1] = createPipSpinner("Select shock pips (0-6). Applied during shock phases.", general.getShock());
		createPipLabel("Maneuver");
		pipSpinners[2] = createPipSpinner("Select maneuver pips. May help cancel out crossing penalties.",
				general.getManeuver());
		createPipLabel("Siege");
		pipSpinners[3] = createPipSpinner("Select siege pips (0-6). Has no influence on combat.", general.getSiege());
		
		return swtGroup;
	}
	
	private void createPipLabel(String pip) {
		Label pipLabel = new Label(swtGroup, SWT.NONE);
		pipLabel.setLayoutData(new RowData(SWT.DEFAULT, SWT.DEFAULT));
		pipLabel.setToolTipText(pip);
		pipLabel.setImage(
				SWTResourceManager.getImage(GeneralGroup.class, "/icons/general/Land_leader_" + pip + ".png"));
	}
	
	private Spinner createPipSpinner(String tooltip, int value) {
		Spinner spinner = new Spinner(swtGroup, SWT.BORDER);
		spinner.setLayoutData(new RowData(SWT.DEFAULT, SWT.DEFAULT));
		spinner.setToolTipText(tooltip);
		spinner.setSelection(value);
		spinner.setMinimum(0);
		spinner.setMaximum(6);
		return spinner;
	}
	
	/**
	 * @return A new {@link General} from the current spinner (fire, shock, maneuver, siege) selections
	 */
	public General getGeneral() {
		int fire = pipSpinners[0].getSelection();
		int shock = pipSpinners[1].getSelection();
		int maneuver = pipSpinners[2].getSelection();
		int siege = pipSpinners[3].getSelection();
		return new General(fire, shock, maneuver, siege);
	}
}
