package eu4.comsim.core.test;

import eu4.comsim.core.Army;
import eu4.comsim.core.Battle;
import eu4.comsim.core.BattleStats;
import eu4.comsim.core.General;
import eu4.comsim.core.Technology;
import eu4.comsim.core.Util;
import eu4.comsim.core.datatypes.Terrain;
import eu4.comsim.core.datatypes.Terrain.CrossingPenalty;

/**
 * For command line Battle testing, easier setup than via GUI
 * 
 */
public class BattleTester {
	
	public static void main(String[] args) {
		
		Technology techA = new Technology(16, Technology.Group.WESTERN, 1.08, 1.293, 1.0, 1.0, 1.0);
		Technology techB = new Technology(16, Technology.Group.WESTERN, 1.05, 1.196, 1.1, 1.0, 1.0);
		Army armyA = new Army(new General(4, 4, 4, 1), techA, Util.getRegiments(techA, 19, 2, 6));
		Army armyB = new Army(new General(3, 2, 3, 0), techB, Util.getRegiments(techB, 12, 5, 2));
		
		Battle b = new Battle(armyA, armyB, Terrain.GRASSLANDS, CrossingPenalty.RIVER);
		b.run();
		
		BattleStats statsA = new BattleStats(b.battleLineA, b.getDuration(), true);
		BattleStats statsB = new BattleStats(b.battleLineB, b.getDuration(), true);
		System.out.println(statsA);
		System.out.println(statsB);
	}
}
